package com.rayvatapps.wallpaperapp;

import android.Manifest;
import android.app.Fragment;
import android.content.Context;
import android.content.pm.PackageManager;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.support.annotation.ColorInt;
import android.support.annotation.ColorRes;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.widget.Toast;

import com.karan.churi.PermissionManager.PermissionManager;
import com.rayvatapps.wallpaperapp.fragment.CategoryFragment;
import com.rayvatapps.wallpaperapp.fragment.CenteredTextFragment;
import com.rayvatapps.wallpaperapp.fragment.FavoriteFragment;
import com.rayvatapps.wallpaperapp.fragment.HomeFragment;
import com.rayvatapps.wallpaperapp.fragment.PrivacyPolicyFragment;
import com.rayvatapps.wallpaperapp.menu.DrawerAdapter;
import com.rayvatapps.wallpaperapp.menu.DrawerItem;
import com.rayvatapps.wallpaperapp.menu.SimpleItem;
import com.rayvatapps.wallpaperapp.menu.SpaceItem;
import com.yarolegovich.slidingrootnav.SlidingRootNav;
import com.yarolegovich.slidingrootnav.SlidingRootNavBuilder;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by Mitesh Makwana on 03/08/18.
 */
public class MainActivity extends AppCompatActivity implements DrawerAdapter.OnItemSelectedListener{
    private Context mContext=MainActivity.this;

    private static final int POS_HOME = 0;
    private static final int POS_CATEGORY = 1;
    private static final int POS_FAVORITE = 2;
    private static final int POS_PRIVACYPOLICY = 3;
    private static final int POS_SHARE = 5;

    private String[] screenTitles;
    private Drawable[] screenIcons;

    private SlidingRootNav slidingRootNav;
    boolean doubleBackToExitPressedOnce = false;

    PermissionManager permissionManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        getpermissions();

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        slidingRootNav = new SlidingRootNavBuilder(MainActivity.this)
                .withToolbarMenuToggle(toolbar)
                .withMenuOpened(false)
                .withContentClickableWhenMenuOpened(false)
                .withSavedState(savedInstanceState)
                .withMenuLayout(R.layout.menu_left_drawer)
                .inject();

        screenIcons = loadScreenIcons();
        screenTitles = loadScreenTitles();

        DrawerAdapter adapter = new DrawerAdapter(Arrays.asList(
                createItemFor(POS_HOME).setChecked(true),
                createItemFor(POS_CATEGORY),
                createItemFor(POS_FAVORITE),
                createItemFor(POS_PRIVACYPOLICY),
                new SpaceItem(48),
                createItemFor(POS_SHARE)));
        adapter.setListener(MainActivity.this);

        RecyclerView list = findViewById(R.id.list);
        list.setNestedScrollingEnabled(false);
        list.setLayoutManager(new LinearLayoutManager(this));
        list.setAdapter(adapter);

        adapter.setSelected(POS_HOME);
    }

    @Override
    public void onItemSelected(int position) {
       /* if (position == POS_LOGOUT) {
            finish();
        }*/
        slidingRootNav.closeMenu();
        Fragment selectedScreen = HomeFragment.createFor(screenTitles[position]);
        if (position == 0) {
            selectedScreen = HomeFragment.createFor(screenTitles[position]);
        }
        else if(position==1) {
            selectedScreen = CategoryFragment.createFor(screenTitles[position]);
        }
        else if(position==2) {
            selectedScreen = FavoriteFragment.createFor(screenTitles[position]);
        }
        else if(position==3) {
            selectedScreen = PrivacyPolicyFragment.createFor(screenTitles[position]);
        }
        else if(position==5) {
            selectedScreen = CenteredTextFragment.createFor(screenTitles[position]);
        }
        showFragment(selectedScreen);
    }

    private void showFragment(Fragment fragment) {
        getFragmentManager().beginTransaction()
                .replace(R.id.container, fragment)
                .commit();
    }

    private DrawerItem createItemFor(int position) {
        return new SimpleItem(screenIcons[position], screenTitles[position])
                .withIconTint(color(R.color.textColorSecondary))
                .withTextTint(color(R.color.textColorPrimary))
                .withSelectedIconTint(color(R.color.colorAccent))
                .withSelectedTextTint(color(R.color.colorAccent));
    }

    private String[] loadScreenTitles() {
        return getResources().getStringArray(R.array.ld_activityScreenTitles);
    }

    private Drawable[] loadScreenIcons() {
        TypedArray ta = getResources().obtainTypedArray(R.array.ld_activityScreenIcons);
        Drawable[] icons = new Drawable[ta.length()];
        for (int i = 0; i < ta.length(); i++) {
            int id = ta.getResourceId(i, 0);
            if (id != 0) {
                icons[i] = ContextCompat.getDrawable(this, id);
            }
        }
        ta.recycle();
        return icons;
    }

    @ColorInt
    private int color(@ColorRes int res) {
        return ContextCompat.getColor(this, res);
    }

    private void getpermissions() {

        permissionManager=new PermissionManager() {

            @Override
            public List<String> setPermission() {
                // If You Don't want to check permission automatically and check your own custom permission
                // Use super.setPermission(); or Don't override this method if not in use
                List<String> customPermission=new ArrayList<>();
                customPermission.add(Manifest.permission.INTERNET);
                customPermission.add(Manifest.permission.ACCESS_NETWORK_STATE);
                customPermission.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
                customPermission.add(Manifest.permission.READ_EXTERNAL_STORAGE);
                return customPermission;
            }
        };

        //To initiate checking permission
        permissionManager.checkAndRequestPermissions(this);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        permissionManager.checkResult(requestCode,permissions,grantResults);

        ArrayList<String> granted=permissionManager.getStatus().get(0).granted;
        ArrayList<String> denied=permissionManager.getStatus().get(0).denied;

        for(String item:granted)
            Log.e("granted",item);

        for(String item:denied)
            Log.e("granted",item);

    }

    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            return;
        }

        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(mContext, "Please click BACK again to exit", Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce=false;
            }
        }, 2000);
    }
}
