package com.rayvatapps.wallpaperapp.adapter;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.rayvatapps.wallpaperapp.R;
import com.rayvatapps.wallpaperapp.SubcategoryActivity;
import com.rayvatapps.wallpaperapp.jsonurl.Config;
import com.rayvatapps.wallpaperapp.model.category;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Created by Mitesh Makwana on 03/08/18.
 */
public class CategoryAdapter extends RecyclerView.Adapter<CategoryAdapter.MyViewHolder> implements Filterable {

    private Context mContext;
    private List<category> categoryList;
    private List<category> categoryListFiltered;

    int[] myImageList = {R.drawable.wallpaper_1, R.drawable.wallpaper_2,R.drawable.wallpaper_3,
            R.drawable.wallpaper_4,R.drawable.wallpaper_5,R.drawable.wallpaper_6};

    Typeface tf;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView name;
        ImageView image;
        CardView cdview;

        public MyViewHolder(View view) {
            super(view);

            name = (TextView) view.findViewById(R.id.txtcategoryname);

            image=(ImageView)view.findViewById(R.id.imgcategory);

            cdview = (CardView) view.findViewById(R.id.card_view);
        }
    }

    public CategoryAdapter(Context mContext, List<category> categoryList) {
        this.mContext = mContext;
        this.categoryList = categoryList;
        this.categoryListFiltered = categoryList;

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.raw_category, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {

        tf = Typeface.createFromAsset(mContext.getAssets(), "fonts/HKNova-Medium.ttf");
        holder.name.setTypeface(tf);

        final category album = categoryListFiltered.get(position);
        holder.name.setText(album.getName());

        int img=myImageList[position%myImageList.length];


        Glide.with(mContext)
                .load(album.getUrl())
//                .error(drawable)
                .apply(new RequestOptions()
                .placeholder(myImageList[5])
                .error(myImageList[position%myImageList.length]))
                .into(holder.image);

        holder.cdview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i=new Intent(mContext,SubcategoryActivity.class);
                i.putExtra("catid", album.getId());
                i.putExtra("catname", album.getName());
                mContext.startActivity(i);
//                ((Activity)mContext).finish();
            }
        });

    }

    @Override
    public int getItemCount() {
        return categoryListFiltered.size();
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    categoryListFiltered = categoryList;
                } else {
                    List<category> filteredList = new ArrayList<>();
                    for (category row : categoryList) {

                        // name match condition. this might differ depending on your requirement
                        // here we are looking for name or phone number match
                        if (row.getName().toLowerCase().contains(charString.toLowerCase()) ) {
                            filteredList.add(row);
                        }
                    }
                    categoryListFiltered = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = categoryListFiltered;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                categoryListFiltered = (ArrayList<category>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }

}