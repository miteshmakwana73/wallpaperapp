package com.rayvatapps.wallpaperapp.adapter;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.rayvatapps.wallpaperapp.R;
import com.rayvatapps.wallpaperapp.SubcategoryActivity;
import com.rayvatapps.wallpaperapp.model.subcategory;

import java.util.List;

public class FullImageAdapter extends PagerAdapter {
    Context mContext;
    List<subcategory> albumList;
    private int[] GalImages = new int[] {
        R.drawable.wallpaper_1,
        R.drawable.wallpaper_2,
        R.drawable.wallpaper_3,
        R.drawable.wallpaper_4,
        R.drawable.wallpaper_5,
        R.drawable.wallpaper_6
    };

    public FullImageAdapter(Context context, List<subcategory> albumList){
        this.mContext=context;
        this.albumList=albumList;
    }

    @Override
    public int getCount() {
    return GalImages.length;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((ImageView) object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        ImageView imageView = new ImageView(mContext);
        int padding = mContext.getResources().getDimensionPixelSize(R.dimen.padding_medium);
        imageView.setPadding(padding, padding, padding, padding);
        imageView.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
//        imageView.setImageResource(SubCategoryAdapter.imageList[position]);

//        Log.e(SubCategoryAdapter.imageList.toString(),String.valueOf(position));
        final subcategory album = albumList.get(position);
        String[] myarray = new String[albumList.size()];
        for (int i = 0; i < albumList.size(); i++) {
            myarray[i]=album.getUrl_image();
//            Log.e("data"+i, myarray[i]);
        }

        Log.e("size = "+albumList.size(),"postition = "+position);
        Glide.with(mContext)
                .load(albumList.get(position).getUrl_image())
                .apply(
                        RequestOptions.placeholderOf(R.drawable.gradient)
                .error(R.drawable.ic_arrow_back_black_24dp))
                .into(imageView);

        ((ViewPager) container).addView(imageView, 0);
        return imageView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        ((ViewPager) container).removeView((ImageView) object);
    }
}