package com.rayvatapps.wallpaperapp.adapter;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.rayvatapps.wallpaperapp.FullImageActivity;
import com.rayvatapps.wallpaperapp.R;
import com.rayvatapps.wallpaperapp.SlideshowDialogFragment;
import com.rayvatapps.wallpaperapp.model.subcategory;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Mitesh Makwana on 06/08/18.
 */
public class SubCategoryAdapter extends RecyclerView.Adapter<SubCategoryAdapter.MyViewHolder> {

    private Context mContext;
    private static List<subcategory> albumList;
    public static List<subcategory> imageList;
    Typeface tf;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        ImageView image;
        CardView cdview;

        public MyViewHolder(View view) {
            super(view);

            image=(ImageView)view.findViewById(R.id.imgsubcategory);

            cdview = (CardView) view.findViewById(R.id.card_view);
        }
    }

    public SubCategoryAdapter(Context mContext, List<subcategory> albumList) {
        this.mContext = mContext;
        this.albumList = albumList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.raw_subcategory, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        tf = Typeface.createFromAsset(mContext.getAssets(), "fonts/HKNova-Medium.ttf");

        imageList=new ArrayList();
        imageList=albumList;
        final subcategory album = albumList.get(position);

        Glide.with(mContext)
                .load(album.getUrl_thumb())
                .into(holder.image);

        holder.cdview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SharedPreferences preference = PreferenceManager.getDefaultSharedPreferences(mContext);
                SharedPreferences.Editor editer = preference.edit();
                editer.putInt("position", position);
                editer.commit();
                Intent i=new Intent(mContext,FullImageActivity.class);
                mContext.startActivity(i);

               /* FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                SlideshowDialogFragment newFragment = SlideshowDialogFragment.newInstance();
//                newFragment.setArguments(bundle);
                newFragment.show(ft, "slideshow");
                Intent i=new Intent(mContext,FullImageActivity.class);
                i.putExtra("position",position);
//                i.putParcelableArrayListExtra("data", albumList);
//                i.putExtra("imagearraylist",  array);
//                i.putStringArrayListExtra("stock_list", albumList);
                mContext.startActivity(i);
//                ((Activity)mContext).finish();

                Intent intent = new Intent(mContext, FullImageActivity.class);
                SharedPreferences preference = PreferenceManager
                        .getDefaultSharedPreferences(mContext);
                SharedPreferences.Editor editer = preference.edit();
                editer.putInt("position", position);
                editer.commit();
                mContext.startActivity(intent);*/
            }
        });
    }

    @Override
    public int getItemCount() {
        return albumList.size();
    }

    public static List<subcategory> getArrayList(){
        return albumList;
    }

    public interface ClickListener {
        void onClick(View view, int position);

        void onLongClick(View view, int position);
    }

    public static class RecyclerTouchListener implements RecyclerView.OnItemTouchListener {

        private GestureDetector gestureDetector;
        private SubCategoryAdapter.ClickListener clickListener;

        public RecyclerTouchListener(Context context, final RecyclerView recyclerView, final SubCategoryAdapter.ClickListener clickListener) {
            this.clickListener = clickListener;
            gestureDetector = new GestureDetector(context, new GestureDetector.SimpleOnGestureListener() {
                @Override
                public boolean onSingleTapUp(MotionEvent e) {
                    return true;
                }

                @Override
                public void onLongPress(MotionEvent e) {
                    View child = recyclerView.findChildViewUnder(e.getX(), e.getY());
                    if (child != null && clickListener != null) {
                        clickListener.onLongClick(child, recyclerView.getChildPosition(child));
                    }
                }
            });
        }

        @Override
        public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {

            View child = rv.findChildViewUnder(e.getX(), e.getY());
            if (child != null && clickListener != null && gestureDetector.onTouchEvent(e)) {
                clickListener.onClick(child, rv.getChildPosition(child));
            }
            return false;
        }

        @Override
        public void onTouchEvent(RecyclerView rv, MotionEvent e) {
        }

        @Override
        public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

        }
    }
}