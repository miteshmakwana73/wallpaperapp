package com.rayvatapps.wallpaperapp.model;
/**
 * Created by Mitesh Makwana on 03/08/18.
 */
public class category {
    private int id;
    private String name;
    private String count;
    private String url;

    public category(int id, String name, String count, String url) {
        this.id = id;
        this.name = name;
        this.count = count;
        this.url = url;
    }

    public category() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCount() {
        return count;
    }

    public void setCount(String count) {
        this.count = count;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
