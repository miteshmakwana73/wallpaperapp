package com.rayvatapps.wallpaperapp.model;

import java.io.Serializable;

/**
 * Created by Mitesh Makwana on 06/08/18.
 */
public class subcategory implements Serializable{
    private int id;
    private String width;
    private String height;
    private String file_type;
    private String file_size;
    private String url_image;
    private String url_thumb;
    private String url_page;

    public subcategory(int id, String width, String height, String file_type, String file_size, String url_image, String url_thumb, String url_page) {
        this.id = id;
        this.width = width;
        this.height = height;
        this.file_type = file_type;
        this.file_size = file_size;
        this.url_image = url_image;
        this.url_thumb = url_thumb;
        this.url_page = url_page;
    }

    public subcategory() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getWidth() {
        return width;
    }

    public void setWidth(String width) {
        this.width = width;
    }

    public String getHeight() {
        return height;
    }

    public void setHeight(String height) {
        this.height = height;
    }

    public String getFile_type() {
        return file_type;
    }

    public void setFile_type(String file_type) {
        this.file_type = file_type;
    }

    public String getFile_size() {
        return file_size;
    }

    public void setFile_size(String file_size) {
        this.file_size = file_size;
    }

    public String getUrl_image() {
        return url_image;
    }

    public void setUrl_image(String url_image) {
        this.url_image = url_image;
    }

    public String getUrl_thumb() {
        return url_thumb;
    }

    public void setUrl_thumb(String url_thumb) {
        this.url_thumb = url_thumb;
    }

    public String getUrl_page() {
        return url_page;
    }

    public void setUrl_page(String url_page) {
        this.url_page = url_page;
    }
}
