package com.rayvatapps.wallpaperapp;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.util.TypedValue;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.rayvatapps.wallpaperapp.adapter.SubCategoryAdapter;
import com.rayvatapps.wallpaperapp.jsonurl.Config;
import com.rayvatapps.wallpaperapp.model.subcategory;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Mitesh Makwana on 06/08/18.
 */
public class SubcategoryActivity extends AppCompatActivity {
    private Context mContext=SubcategoryActivity.this;

    private RecyclerView recyclerView;
    private SubCategoryAdapter adapter;
    public static List<subcategory> ImageList;
    ProgressBar progressBar;

    SwipeRefreshLayout sw_refresh;

    String HttpUrl = Config.SERVER_URL+Config.WALLPAPER_API_AUTHENTICATION_KEY+"&method=category&id=";

    RelativeLayout rlnavigation;
    FloatingActionButton fabNext,fabPrevious;
    RelativeLayout rlerror;
    TextView status,tverror;
    ImageView errorimage;
    Toolbar toolbar;

    int categoryId,pageCount=1;
    String categoryName;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_subcategory);

        Bundle bundle= getIntent().getExtras();

        Bundle extras = getIntent().getExtras();
        if(extras == null) {
            categoryId= 0;
            categoryName="";
        } else {
            categoryId= bundle.getInt("catid");
            categoryName= bundle.getString("catname");
        }

        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        progressBar = (ProgressBar)findViewById(R.id.progressBar);
        rlnavigation=(RelativeLayout)findViewById(R.id.rldfabnavigation);
        fabNext=(FloatingActionButton)findViewById(R.id.fabnext);
        fabPrevious=(FloatingActionButton)findViewById(R.id.fabprevious);
        rlerror=(RelativeLayout) findViewById(R.id.rlerror);
        status=(TextView)findViewById(R.id.tvstatus);
        errorimage=(ImageView) findViewById(R.id.imgerror);
        tverror=(TextView)findViewById(R.id.tverror);
        sw_refresh = (SwipeRefreshLayout) findViewById(R.id.sw_refresh);

        rlnavigation.setVisibility(View.VISIBLE);
        Typeface tf = Typeface.createFromAsset(getAssets(), "fonts/HKNova-Medium.ttf");
        status.setTypeface(tf);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle(categoryName);
        setSupportActionBar(toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back_black_24dp);

        final ActionBar ab = getSupportActionBar();
        ab.setDisplayHomeAsUpEnabled(true);


        sw_refresh = (SwipeRefreshLayout) findViewById(R.id.sw_refresh);

        sw_refresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                new Handler().postDelayed(new Runnable() {
                    public void run() {
                        // do something...

                        ImageList.clear();

                        adapter.notifyDataSetChanged();

                        checkconnection();

                        adapter.notifyDataSetChanged();

                        sw_refresh.setRefreshing(false);

                    }
                }, 1000);
            }
        });

        fabPrevious.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pageCount-=1;
                checkconnection();
            }
        });

        fabNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pageCount+=1;
                checkconnection();
            }
        });

        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);

        ImageList = new ArrayList<>();

        adapter = new SubCategoryAdapter(this,ImageList);

        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(this, 2);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.addItemDecoration(new GridSpacingItemDecoration(2, dpToPx(10), true));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adapter);

        /*recyclerView.addOnItemTouchListener(new SubCategoryAdapter.RecyclerTouchListener(getApplicationContext(), recyclerView, new SubCategoryAdapter.ClickListener() {
            @Override
            public void onClick(View view, int position) {
                *//*Bundle bundle = new Bundle();
//                bundle.putSerializable("images", ImageList);
                bundle.putInt("position", position);*//*
                SharedPreferences preference = PreferenceManager.getDefaultSharedPreferences(mContext);
                SharedPreferences.Editor editer = preference.edit();
                editer.putInt("position", position);
                editer.commit();

                FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                SlideshowDialogFragment newFragment = SlideshowDialogFragment.newInstance();
//                newFragment.setArguments(bundle);
                newFragment.show(ft, "slideshow");
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));*/

        if(pageCount==1)
        {
            fabPrevious.setVisibility(View.GONE);
        }
        else
        {
            fabPrevious.setVisibility(View.VISIBLE);
        }
        checkconnection();
    }

    private void checkconnection() {
        rlerror.setVisibility(View.GONE);
        ImageList.clear();
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        android.net.NetworkInfo wifi = cm
                .getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        android.net.NetworkInfo datac = cm
                .getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
        if ((wifi != null & datac != null)
                && (wifi.isConnected() | datac.isConnected())) {

            rlerror.setVisibility(View.GONE);
            rlnavigation.setVisibility(View.VISIBLE);

            loadSubCategoryImaes();
            //connection is avlilable
        }else {
            //no connection
            //displayAlert();
            status.setText(R.string.noconnection);
//            status.setVisibility(View.VISIBLE);
            rlerror.setVisibility(View.VISIBLE);
            rlnavigation.setVisibility(View.GONE);
            /*errorimage.setBackgroundResource(R.drawable.charge);
            tverror.setText(R.string.noconnection);*/
        }
    }

    private void loadSubCategoryImaes() {
        progressBar.setVisibility(View.VISIBLE);

        // Creating string request with post method.
        StringRequest stringRequest = new StringRequest(Request.Method.POST, HttpUrl+categoryId+"&page="+pageCount,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String ServerResponse) {

                        // Hiding the progress dialog after all task complete.
                        progressBar.setVisibility(View.GONE);
                        try {
                            if(pageCount!=1)
                            {
                                fabPrevious.setVisibility(View.VISIBLE);
                            }
                            Log.e("responce",ServerResponse);

                            JSONObject jobj = new JSONObject(ServerResponse);

                            String status1 = jobj.getString("success");

//                            String msg = jobj.getString("message");

                            if (status1.equals("true")) {

                                JSONArray heroArray = jobj.getJSONArray("wallpapers");

                                //now looping through all the elements of the json array
                                for (int i = 0; i < heroArray.length(); i++) {
                                    //getting the json object of the particular index inside the array
                                    JSONObject heroObject = heroArray.getJSONObject(i);

                                    //creating a hero object and giving them the values from json object

                                    subcategory hero = new subcategory(
                                            heroObject.getInt("id"),
                                            heroObject.getString("width"),
                                            heroObject.getString("height"),
                                            heroObject.getString("file_type"),
                                            heroObject.getString("file_size"),
                                            heroObject.getString("url_image"),
                                            heroObject.getString("url_thumb"),
                                            heroObject.getString("url_page"));

                                    //adding the hero to herolist
                                    ImageList.add(hero);
                                }
                                //Log.e("list", String.valueOf(albumList));
                                //creating custom adapter object
                                adapter = new SubCategoryAdapter(mContext,ImageList);

                                //adding the adapter to listview
                                recyclerView.setAdapter(adapter);


                            } else {
                                rlerror.setVisibility(View.VISIBLE);
                                errorimage.setBackground(getResources().getDrawable(R.drawable.no_data));
                                status.setText(getResources().getString(R.string.nodata));
//                               status.setVisibility(View.VISIBLE);
//                                Toast.makeText(SubcategoryActivity.this, msg, Toast.LENGTH_SHORT).show();

                            }

                        }catch (Exception e) {
                            e.printStackTrace();
                            Log.e("error", String.valueOf(e));
                        }

                        // Showing response message coming from server.

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {

                        // Hiding the progress dialog after all task complete.
                        progressBar.setVisibility(View.GONE);

                        if( volleyError instanceof NoConnectionError) {
                            showToast("No connection available");
                        }
                        else if (volleyError.getClass().equals(TimeoutError.class)) {
                            // Show timeoutbuttonSignup error message
                            showToast("Oops. Timeout error!");
                        }
                        else if (volleyError instanceof ServerError) {
                            // Show timeout error message
                            showToast("Server error!");
                        }
                        else
                        {
                            showToast(volleyError.toString());
                        }

                    }
                }) ;

        // Creating RequestQueue.
        RequestQueue requestQueue = Volley.newRequestQueue(mContext);

        // Adding the StringRequest object into requestQueue.
        requestQueue.add(stringRequest);
    }

    private void showToast(String msg) {
        Toast.makeText(mContext, msg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * RecyclerView item decoration - give equal margin around grid item
     */
    public class GridSpacingItemDecoration extends RecyclerView.ItemDecoration {

        private int spanCount;
        private int spacing;
        private boolean includeEdge;

        public GridSpacingItemDecoration(int spanCount, int spacing, boolean includeEdge) {
            this.spanCount = spanCount;
            this.spacing = spacing;
            this.includeEdge = includeEdge;
        }

        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
            int position = parent.getChildAdapterPosition(view); // item position
            int column = position % spanCount; // item column

            if (includeEdge) {
                outRect.left = spacing - column * spacing / spanCount; // spacing - column * ((1f / spanCount) * spacing)
                outRect.right = (column + 1) * spacing / spanCount; // (column + 1) * ((1f / spanCount) * spacing)

                if (position < spanCount) { // top edge
                    outRect.top = spacing;
                }
                outRect.bottom = spacing; // item bottom
            } else {
                outRect.left = column * spacing / spanCount; // column * ((1f / spanCount) * spacing)
                outRect.right = spacing - (column + 1) * spacing / spanCount; // spacing - (column + 1) * ((1f /    spanCount) * spacing)
                if (position >= spanCount) {
                    outRect.top = spacing; // item top
                }
            }
        }
    }

    /**
     * Converting dp to pixel
     */
    private int dpToPx(int dp) {
        Resources r = getResources();
        return Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, r.getDisplayMetrics()));
    }
}
