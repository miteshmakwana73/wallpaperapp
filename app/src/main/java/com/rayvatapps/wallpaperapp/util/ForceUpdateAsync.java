package com.rayvatapps.wallpaperapp.util;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.v7.app.AlertDialog;
import android.view.ContextThemeWrapper;


import com.rayvatapps.wallpaperapp.R;

import org.json.JSONObject;

import java.io.IOException;

public class ForceUpdateAsync extends AsyncTask<String, String, JSONObject> {

    private String latestVersion;
    private String currentVersion;
    private Context context;
    public ForceUpdateAsync(String currentVersion, Context context){
        this.currentVersion = currentVersion;
        this.context = context;
    }


    public static void showForceUpdateDialog(final Context context, String version, String froce_update, String recommend_update){
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(new ContextThemeWrapper(context,
                R.style.AlertDialog_AppCompat_Light));

        alertDialogBuilder.setTitle(context.getString(R.string.youAreNotUpdatedTitle));
        alertDialogBuilder.setMessage(context.getString(R.string.youAreNotUpdatedMessagenew) + " " + version +" "+ context.getString(R.string.youAreNotUpdatedMessage1));
        alertDialogBuilder.setPositiveButton(R.string.update, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + context.getPackageName())));
                dialog.cancel();
            }
        });


        if(recommend_update.equals("1"))
        {
            alertDialogBuilder.show();
            alertDialogBuilder.setCancelable(true);//close allow
        }
        else
        {
            alertDialogBuilder.setCancelable(false);//close not allow
        }
        if(froce_update.equals("1"))
        {
            alertDialogBuilder.show();
        }

    }

    @Override
    protected JSONObject doInBackground(String... strings) {
        return null;
    }
}