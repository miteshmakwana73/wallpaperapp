package com.rayvatapps.wallpaperapp.jsonurl;
/**
 * Created by Mitesh Makwana on 03/08/18.
 */
public class Config {

    public static final String SERVER_URL="https://wall.alphacoders.com/api2.0/get.php?auth=";

    public static final String LOGIN_URL= SERVER_URL+"login";

    public static final String LEAVE_ADD_URL= SERVER_URL+"add_leave";
    public static final String LEAVE_VIEW_URL= SERVER_URL+"leave_requests";
    public static final String LEAVE_DELETE_URL= SERVER_URL+"delete_leave";
    public static final String LEAVE_BALANCE_URL= SERVER_URL+"leave_balance";
    public static final String LEAVE_TYPE_URL= SERVER_URL+"leave_types";
    public static final String UPDATE_APP= SERVER_URL+"app_version";

    public static final String USER_IMG_PATH= "http://rayvatapps.com:8002/assets/uploads/avatar/";

    // global topic to receive app wide push notifications
    public static final String WALLPAPER_API_AUTHENTICATION_KEY = "509600488ba6943d618bd01e1176515b";

    // broadcast receiver intent filters
    public static final String REGISTRATION_COMPLETE = "registrationComplete";
    public static final String PUSH_NOTIFICATION = "pushNotification";

    // id to handle the notification in the notification tray
    public static final int NOTIFICATION_ID = 100;
    public static final int NOTIFICATION_ID_BIG_IMAGE = 101;

    public static final String SHARED_PREF = "ah_firebase";
}