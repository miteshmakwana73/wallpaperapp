package com.rayvatapps.wallpaperapp;

import android.Manifest;
import android.app.Dialog;
import android.app.DownloadManager;
import android.app.NotificationManager;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.provider.SyncStateContract;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.NotificationCompat;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions;
import com.github.clans.fab.FloatingActionButton;
import com.github.clans.fab.FloatingActionMenu;
import com.rayvatapps.wallpaperapp.adapter.FullImageAdapter;
import com.rayvatapps.wallpaperapp.adapter.SubCategoryAdapter;
import com.rayvatapps.wallpaperapp.model.subcategory;

import java.io.BufferedInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Mitesh Makwana on 07/08/18.
 */
public class FullImageActivity extends AppCompatActivity
{
    private String TAG = FullImageActivity.class.getSimpleName();
    private Context mContext=FullImageActivity.this;

    int position=0;
    private static  List<subcategory> albumList;
    private ViewPager viewPager;
    private MyViewPagerAdapter myViewPagerAdapter;

    private FloatingActionMenu menuOption;
    private FloatingActionButton fabDownload,fabShare,fabCrop,fabWallpaper;

    private DownloadManager downloadManager;
    private long refid;
    private Uri Download_Uri;
    ArrayList<Long> list = new ArrayList<>();

    private ProgressDialog pDialog;
    public static final int progress_bar_type = 0;
    private static String file_url = "http://api.androidhive.info/progressdialog/hive.jpg";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_full_image);
        albumList = new ArrayList<>();
        // Hide status bar
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
//        // Show status bar
//        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);

        viewPager = (ViewPager) findViewById(R.id.viewpager);
        /*lblCount = (TextView) v.findViewById(R.id.lbl_count);
        lblTitle = (TextView) v.findViewById(R.id.title);
        lblDate = (TextView) v.findViewById(R.id.date);*/

        albumList=(ArrayList<subcategory>)SubcategoryActivity.ImageList;

        SharedPreferences prefernce = PreferenceManager.getDefaultSharedPreferences(mContext);
        position = prefernce.getInt("position", 0);
//        selectedPosition = getArguments().getInt("position");

        Log.e(TAG, "position: " + position);
        Log.e(TAG, "images size: " + albumList.size());

        myViewPagerAdapter = new MyViewPagerAdapter();
        viewPager.setAdapter(myViewPagerAdapter);
        viewPager.addOnPageChangeListener(viewPagerPageChangeListener);

        menuOption = (FloatingActionMenu) findViewById(R.id.menu_option);
        menuOption.setClosedOnTouchOutside(true);
        fabDownload=(FloatingActionButton) findViewById(R.id.fabdownload);
        fabShare=(FloatingActionButton) findViewById(R.id.fabshare);
        fabCrop=(FloatingActionButton) findViewById(R.id.fabcrop);
        fabWallpaper=(FloatingActionButton) findViewById(R.id.fabwallpaper);
        downloadManager = (DownloadManager) getSystemService(Context.DOWNLOAD_SERVICE);

        registerReceiver(onComplete,
                new IntentFilter(DownloadManager.ACTION_DOWNLOAD_COMPLETE));

        if(!isStoragePermissionGranted())
        {


        }

        fabDownload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                list.clear();
                String folderName=getResources().getString(R.string.app_name);
                DownloadManager.Request request = new DownloadManager.Request(Download_Uri);
                request.setAllowedNetworkTypes(DownloadManager.Request.NETWORK_WIFI | DownloadManager.Request.NETWORK_MOBILE);
                request.setAllowedOverRoaming(false);
                request.setTitle("Downloading");
                request.setDescription("image");
                request.setVisibleInDownloadsUi(true);
                request.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, "/"+folderName+"/"  + "/" + "Sample" + ".png");

                refid = downloadManager.enqueue(request);
                showDialog();
                Log.e("OUT", "" + refid);

                list.add(refid);

//                new DownloadFileFromURL().execute(file_url);

                showToast("Downloading...");

                menuOption.close(true);
            }
        });

        setCurrentItem(position);


    }

    private void showToast(String msg) {
        Toast.makeText(mContext, msg, Toast.LENGTH_SHORT).show();
    }
    /**
     * Showing Dialog
     * */
    @Override
    protected Dialog onCreateDialog(int id) {
        switch (id) {
            case progress_bar_type:
                pDialog = new ProgressDialog(this);
                pDialog.setMessage("Downloading file. Please wait...");
                pDialog.setIndeterminate(false);
                pDialog.setMax(100);
                pDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
                pDialog.setCancelable(true);
                pDialog.show();
                return pDialog;
            default:
                return null;
        }
    }

    private void showDialog() {
        final ProgressDialog pDialog;

        pDialog = new ProgressDialog(this);
        pDialog.setMessage("Downloading file. Please wait...");
        pDialog.setIndeterminate(false);
        pDialog.setMax(100);
        pDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        pDialog.setCancelable(true);
        pDialog.show();

        new Thread(new Runnable() {

            @Override
            public void run() {

                boolean downloading = true;

                while (downloading) {

                    DownloadManager.Query q = new DownloadManager.Query();
                    q.setFilterById(refid);

                    Cursor cursor = downloadManager.query(q);
                    cursor.moveToFirst();
                    int bytes_downloaded = cursor.getInt(cursor
                            .getColumnIndex(DownloadManager.COLUMN_BYTES_DOWNLOADED_SO_FAR));
                    int bytes_total = cursor.getInt(cursor.getColumnIndex(DownloadManager.COLUMN_TOTAL_SIZE_BYTES));

                    if (cursor.getInt(cursor.getColumnIndex(DownloadManager.COLUMN_STATUS)) == DownloadManager.STATUS_SUCCESSFUL) {
                        downloading = false;
                    }

//                    final double dl_progress = (bytes_downloaded / bytes_total) * 100;
                    final int dl_progress = (int) ((bytes_downloaded * 100l) / bytes_total);

                    runOnUiThread(new Runnable() {

                        @Override
                        public void run() {

                            pDialog.setProgress((int) dl_progress);
                            if(dl_progress==100)
                                pDialog.dismiss();

                        }
                    });
                    statusMessage(cursor);
//                    Log.d(Constants.MAIN_VIEW_ACTIVITY, statusMessage(cursor));
                    cursor.close();
                }

            }
        }).start();
    }

    private String statusMessage(Cursor c) {
        String msg = "???";

        switch (c.getInt(c.getColumnIndex(DownloadManager.COLUMN_STATUS))) {
            case DownloadManager.STATUS_FAILED:
                msg = "Download failed!";
                break;

            case DownloadManager.STATUS_PAUSED:
                msg = "Download paused!";
                break;

            case DownloadManager.STATUS_PENDING:
                msg = "Download pending!";
                break;

            case DownloadManager.STATUS_RUNNING:
                msg = "Download in progress!";
                break;

            case DownloadManager.STATUS_SUCCESSFUL:
                msg = "Download complete!";
                break;

            default:
                msg = "Download is nowhere in sight";
                break;
        }

        return (msg);
    }

    private void setCurrentItem(int position) {
        viewPager.setCurrentItem(position, false);
//        displayMetaInfo(selectedPosition);
    }

    //	page change listener
    ViewPager.OnPageChangeListener viewPagerPageChangeListener = new ViewPager.OnPageChangeListener() {

        @Override
        public void onPageSelected(int position) {
            /*displayMetaInfo(position);*/
        }

        @Override
        public void onPageScrolled(int arg0, float arg1, int arg2) {

        }

        @Override
        public void onPageScrollStateChanged(int arg0) {

        }
    };

    //	adapter
    public class MyViewPagerAdapter extends PagerAdapter /*implements OnClickableAreaClickedListener*/
    {

        private LayoutInflater layoutInflater;

        public MyViewPagerAdapter() {
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {

            layoutInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View view = layoutInflater.inflate(R.layout.raw_image_fullscreen_preview, container, false);

            ImageView imageViewPreview = (ImageView) view.findViewById(R.id.image_preview);

//            ClickableAreasImage clickableAreasImage = new ClickableAreasImage(new PhotoViewAttacher(imageViewPreview), this);

            subcategory image = albumList.get(position);
            Log.e(String.valueOf(albumList.get(position)),String.valueOf(position));

            Glide.with(mContext).load(image.getUrl_image())
                    .thumbnail(0.5f)
                    .transition(new DrawableTransitionOptions().crossFade())
//                    .apply(RequestOptions.diskCacheStrategy(DiskCacheStrategy.ALL))
                    .into(imageViewPreview);


            subcategory imageDownload = null;

                    if(position!=0 && position!=albumList.size()-1)
                    {
                        imageDownload=albumList.get(position-1);
                    }
                    if(position==albumList.size()-1)
                    {
//                        Log.e("Last","Last");
                        imageDownload=albumList.get(position+1);
                    }
                    else
                    {
                        imageDownload=image;
                    }

            Download_Uri = Uri.parse(imageDownload.getUrl_image());

            container.addView(view);

            return view;
        }

        @Override
        public int getCount() {
            return albumList.size();
        }

        @Override
        public boolean isViewFromObject(View view, Object obj) {
            return view == ((View) obj);
        }


        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((View) object);
        }

        /*@Override
        public void onClickableAreaTouched(Object o) {

        }*/
    }

    public  boolean isStoragePermissionGranted() {
        if (Build.VERSION.SDK_INT >= 23) {
            if (checkSelfPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    == PackageManager.PERMISSION_GRANTED) {
                return true;
            } else {

                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
                return false;
            }
        }
        else { //permission is automatically granted on sdk<23 upon installation
            return true;
        }
    }

    BroadcastReceiver onComplete = new BroadcastReceiver() {

        public void onReceive(Context ctxt, Intent intent) {

            long referenceId = intent.getLongExtra(DownloadManager.EXTRA_DOWNLOAD_ID, -1);

            Log.e("IN", "" + referenceId);

            list.remove(referenceId);

            if (list.isEmpty())
            {

                Log.e("INSIDE", "" + referenceId);
                NotificationCompat.Builder mBuilder =
                        new NotificationCompat.Builder(mContext)
                                .setSmallIcon(R.mipmap.ic_launcher)
                                .setContentTitle(getResources().getString(R.string.app_name))
                                .setContentText("Download completed");

                NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                notificationManager.notify(455, mBuilder.build());

            }

        }
    };

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(onComplete);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if(grantResults[0]== PackageManager.PERMISSION_GRANTED){


            // permission granted

        }
    }

}